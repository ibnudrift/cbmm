<?php
session_start();
error_reporting(E_ALL & ~E_NOTICE);
// error_reporting(0);

include 'get_setting.php';

require_once __DIR__.'/../vendor/autoload.php';
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

$app = new Silex\Application();

/* Global constants */
define('ROOT_PATH', $_SERVER['DOCUMENT_ROOT']);
define('APP_PATH', dirname(ROOT_PATH).DIRECTORY_SEPARATOR.'app'.DIRECTORY_SEPARATOR);
define('ASSETS_PATH', ROOT_PATH.DIRECTORY_SEPARATOR);

// Register Twig
$app->register(new Silex\Provider\TwigServiceProvider(), array(
    'twig.path' => __DIR__.'/views',
));

// Register Swiftmailer
$app->register(new Silex\Provider\SwiftmailerServiceProvider());

// Register URL Generator
$app->register(new Silex\Provider\UrlGeneratorServiceProvider());

// Register Validator
$app->register(new Silex\Provider\ValidatorServiceProvider());

$app->before(function (Request $request) use ($app) {
    if (!isset($_GET['lang'])) {
        return $app->redirect($app['url_generator']->generate('homepage').'?lang=en');
    }

    $app['twig']->addGlobal('lang_active', $_GET['lang']);

    $urls = $_SERVER['REQUEST_URI'];
    $urls = substr($urls, 0, -8);
    $app['twig']->addGlobal('current_page_name', $urls);

    // call text dual language
    $lang_message = array();
    if (isset($_GET['lang'])) {
        if ($_GET['lang'] == 'id') {
            $lang_message = include('lang/id/app.php');
        } else {
            $lang_message = include('lang/en/app.php');
        }
    }

    $app["twig"]->addGlobal("lang_message", $lang_message);
});

$app->before(function (Request $request) use ($app) {
    $app['twig']->addGlobal('current_pages', $request->getRequestUri());
});

$app["twig"]->addGlobal("list_product", $list_product);


// ------------------ Homepage ------------------------
$app->get('/', function () use ($app) {

    $msgs = isset($_GET['msg'])? $_GET['msg'] : '';
    
	return $app['twig']->render('page/home.twig', array(
        'layout' => 'layouts/column1.twig',
        'msg'=>$msgs,
    ));
})
->bind('homepage');

// ------------------ About ------------------
$app->get('/about', function () use ($app) {
    return $app['twig']->render('page/about.twig', array(
        'layout' => 'layouts/inside.twig',
    ));
})
->bind('about');

// ------------------ Project ------------------
$app->get('/project', function () use ($app) {
    return $app['twig']->render('page/project.twig', array(
        'layout' => 'layouts/inside.twig',
    ));
})
->bind('project');

// ------------------ Service ------------------
$app->get('/service', function () use ($app) {
    return $app['twig']->render('page/service.twig', array(
        'layout' => 'layouts/inside.twig',
    ));
})
->bind('service');

// ------------------ Facilities ------------------
$app->get('/facility', function () use ($app) {
    return $app['twig']->render('page/facility.twig', array(
        'layout' => 'layouts/inside.twig',
    ));
})
->bind('facility');

// ------------------ contact ------------------
$app->match('/contact', function (Request $request) use ($app) {

    $data = $request->get('Contact');
    if ($data == null) {
        $data = array(
            'name'=>'',
            'phone'=>'',
            'company'=>'',
            'email'=>'',
            'address'=>'',
            'message'=>'',
        );
    }

    if ($_POST) {

         if (!isset($_POST['g-recaptcha-response'])) {
            return $app->redirect($app['url_generator']->generate('contact').'?msg=error_message&lang='.$_GET['lang']);
        }
        $secret_key = "6LdvsxwUAAAAAEWFx0JBJODNKG-8rfTy08vwhozT"; 
        $response=file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=".$secret_key."&response=".$_POST['g-recaptcha-response']."&remoteip=".$_SERVER['REMOTE_ADDR']);
        
        $response = json_decode($response);
        if($response->success==false)
        {
          return $app->redirect($app['url_generator']->generate('contact').'?msg=error_message&lang='.$_GET['lang']);
        }else{

        $constraint = new Assert\Collection( array(
            'name' => new Assert\NotBlank(),
            'email' => array(new Assert\Email(), new Assert\NotBlank()),
            'phone' => new Assert\Length(array('max'=>2000)),
            'company' => new Assert\Length(array('max'=>2000)),
            'address' => new Assert\Length(array('max'=>2000)),
            // 'city' => new Assert\Length(array('max'=>2000)),
            'message' => new Assert\Length(array('max'=>2000)),
        ) );

        $errors = $app['validator']->validateValue($data, $constraint);

        $errorMessage = array();
        if (count($errors) > 0) {
            foreach ($errors as $error) {
                $errorMessage[] = $error->getPropertyPath().' '.$error->getMessage();
            }
        } else {
             // $app['swiftmailer.options'] = array(
             //        'host' => 'mail.cbmm.com',
             //        'port' => '25',  
             //        'username' => 'no-reply@cbmm.com',
             //        'password' => '6p^8k_ho7qFC',
             //        'encryption' => null,
             //        'auth_mode' => login
             //    );
            
            $pesan = \Swift_Message::newInstance()
                ->setSubject('Contact Form Website Cosmic BMM')
                ->setFrom(array('no-reply@cbmm.com'))
                ->setTo( array('info@cbmm.com', $data['email']) )
                ->setBcc( array('deoryzpandu@gmail.com', 'ibnu@markdesign.net') )
                ->setReplyTo(array('info@cbmm.com'))
                ->setBody($app['twig']->render('page/mail.twig', array(
                    'data' => $data,
                )), 'text/html');

            $app['mailer']->send($pesan); 
            return $app->redirect($app['url_generator']->generate('contact').'?msg=success&lang='.$_GET['lang']);
            }
        }
        // else captcha
    }

    return $app['twig']->render('page/contactus.twig', array(
        'layout' => 'layouts/inside.twig',
        'error' => isset($errorMessage)? $errorMessage : null,
        'data' => $data,
        'msg' => isset($_GET['msg'])? $_GET['msg'] : null,
    ));
})
->bind('contact');

// ------------------ s_enquire ------------------
$app->match('/s_enquire', function (Request $request) use ($app) {
    $data = $request->get('Enquire');
    if ($data == null) {
        $data = array(
            'name'=>'',
            'email'=>'',
            'phone'=>'',
        );
    }

    if ($_POST) {
         if (!isset($_POST['g-recaptcha-response'])) {
            return $app->redirect($app['url_generator']->generate('homepage').'?msg=error_message&lang='.$_GET['lang']);
        }
        $secret_key = "6LdvsxwUAAAAAEWFx0JBJODNKG-8rfTy08vwhozT";
        $response=file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=".$secret_key."&response=".$_POST['g-recaptcha-response']."&remoteip=".$_SERVER['REMOTE_ADDR']);
        
        $response = json_decode($response);
        if($response->success==false)
        {
          return $app->redirect($app['url_generator']->generate('homepage').'?msg=error_message&lang='.$_GET['lang']);
        }else{

        $constraint = new Assert\Collection( array(
            'name' => new Assert\NotBlank(),
            'email' => array(new Assert\Email(), new Assert\NotBlank()),
            'phone' => new Assert\Length(array('max'=>2000)),
        ) );

        $errors = $app['validator']->validateValue($data, $constraint);

        $errorMessage = array();
        if (count($errors) > 0) {
            foreach ($errors as $error) {
                $errorMessage[] = $error->getPropertyPath().' '.$error->getMessage();
            }
        } else {
             $app['swiftmailer.options'] = array(
                    'host' => 'mail.beautykasatama.com',
                    'port' => '25',  
                    'username' => 'no-reply@beautykasatama.com',
                    'password' => '6p^8k_ho7qFC',
                    'encryption' => null,
                    'auth_mode' => login
                ); 

            $pesan = \Swift_Message::newInstance() 
                ->setSubject('Inquire Form Website CV. Beauty Kasatama')
                ->setFrom(array('no-reply@beautykasatama.com'))
                ->setTo( array('info@beautykasatama.com', $data['email']) )
                ->setBcc( array('deoryzpandu@gmail.com', 'ibnu@markdesign.net') )
                ->setReplyTo(array('info@beautykasatama.com'))
                ->setBody($app['twig']->render('page/mail3.twig', array(
                    'data' => $data,
                )), 'text/html');

            $app['mailer']->send($pesan); 
            return $app->redirect($app['url_generator']->generate('homepage').'?msg=success&lang='.$_GET['lang']);
            }
        }
        // else captcha
    }else{
        return $app->redirect($app['url_generator']->generate('homepage'));
    }
})
->bind('s_enquire');

$app['debug'] = true;

$app->run();