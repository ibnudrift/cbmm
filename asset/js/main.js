$(function(){

	var obj_slide = $('#examp_slide');
	var obj_nav = $('.nav_btms_slide ul li a');
	
	$(obj_nav).on('click', function(){
		var nval = parseInt( $(this).attr('data-id') );
		$(obj_slide).carousel(nval);
		
		// active menu
		$('.nav_btms_slide ul li').removeClass('active');
		$(this).parent().addClass('active');

		console.log(nval); return false;
	});

	// run slider function
	var totalItems = $('#examp_slide .carousel-item').length;
	$(obj_slide).on('slid.bs.carousel', function () {
		currentIndex = $('#examp_slide div.active').index() + 1;

		// change active menu
		$('.nav_btms_slide ul li').removeClass('active');
		$('.nav_btms_slide ul li.data-'+ currentIndex).addClass('active');
		// console.log(''+currentIndex+'/'+totalItems+'');
	});
	
});