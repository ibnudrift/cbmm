<?php 

return array(
		'home' => 'HOME',
		'about' => 'ABOUT US',
		'products' => 'PRODUCTS',
		'quality' => 'QUALITY',
		'contact' => 'CONTACT',

		'tagline_header_t1'=>'Disposable Non Woven Products<br>Professionally Designed To Maximise <b>Protection & Comfort</b>',

		// HOME
		't_fcs_m' => 'Beauty Kasatama is committed to provide comprehensive professional grade filtration products with assured sterilization that meets the international guidelines for effective sterilization.',
		'lebih_lanjut' => 'Read More',

		't_banner_hm_left' => 'Fill out your contact information and let our representative handle your inquiries.',
		'your_name' => 'YOUR NAME',
		'email' => 'EMAIL',
		'mobile_number' => 'MOBILE NUMBER',

		'hm_banner_link1' => 'VIEW OUR PRODUCT RANGE',
		'hm_banner_link2' => 'SEE OUR QUALITY PROCESS',
		'hm_banner_link3' => 'visit DIAPRO HEALTHCARE&rsquo;s factory',

		't_bottom_home_banner' => 'What Makes Beauty Kasatama&rsquo;s Products<br /><b>better than the rest</b>',

		't_bottom_home_bann1' => 'Fully Customised<br />Product Upon Request',
		't_bottom_home_bann2' => 'Proven Service<br />Professionalism',
		't_bottom_home_bann3' => 'Always ahead<br />in &nbsp;technology',
		
		't_bottom_home_bann_quality' => 'Our Quality Certification',

		't_bottom_homblue_p' => 'Beauty Kasatama&rsquo;s competitive power lies in our commitment to be the best<br />to <b>respond</b> and <b>exceed</b> the international standard of quality perfection.',
		't_bottom_homblue_link' => 'Let&rsquo;s have a discussion, Click Here',

		// about
		'ABOUT' => 'ABOUT',
		
		'about1_t_left' =>'<h6>First established on the year 2003 at Kapasari - Surabaya, Beauty Kasatama was then a small home industry producing multi purpose masks that was targeted to lower class society that spent the daily activity trapped in the polluted traffic that can harm their health.</h6>
							<span>With a desire to improve the public health, Beauty Kasatama produces economic yet sustainable products that has impact to its&#39; user.</span>',
		'about1_t_right' =>'<p><em>We distinguishes our company with professionalism, and commitment to not just focusing on seeking profits, instead we are desired to improve the society as our long-term plan.</em></p>',

		'about2_t_right' => '<p>Slowly but sure, the company&#39;s passion to bring out the best product for health conscious customers is recognized by the society and therefore the company has grown its&#39; size and capacity. At year 2009, Beauty Kasatama has expanded and succeed to establish a second production warehouse to cover around one third of the nation&#39;s domestic request for health - medical, and hygiene related products made from non woven.</p>
							<p><strong>We are now one of the market leader in our country Indonesia, and we have worked with business partners internationally to distribute our product and did not rule out the possibility for a custom developed products, made just for a specific request.</strong></p>
							<p>We are now aiming to build larger network of business that spread internationally. We are optimist with our chances and we are confident that with consistently producing great product quality, it will bring our brand awareness by itself.</p>',

		't_vision'=>'VISION',
		'tdesc_vision'=>'<p>To be the high-end market leader that sets the standard of quality non woven processed products - build a respected brand that is recognised for its` quality and consistency.</p>',

		't_mission'=>'MISSION',
		'tdesc_mission'=>'<p>Promotes the use of Non Woven material as the sustainable material of choice, targeted to the healthcare, medical and hygiene related industry while continuously expand the industry that focused on bringing impacts to comprehensive public health, and be the supplier of first choice to all our customers.</p>',

		'about_bottom_black_title' => 'WE ARE<br />
										<strong>BEAUTY KASATAMA</strong><br />
										AND WE&rsquo;RE HERE<br />
										TO HELP YOU',
		'about_bottom_black_desc' => '<p><strong>Front Row, from left to right:</strong><br />
										Tan Roy Tanusudiro - Marketing Director, Lim Kevin Gartnedi Halim - Operational Director, Audi Pascalis Umboh - Head of Export Division</p>

										<p><strong>Back Row, from left to right:</strong><br />
										Management Team - Ahmad Fathoni, Ahmad Nurudin, Abdul Manaf, Nurchamid, Moelyono</p>',
		// product

		't_left_product' => '<h1>OUR<br><b>PRODUCTS</b></h1>
					<div class="clear height-10"></div>
					<h6>A strategic regional network of distribution centres with immediate stock availability are the keys to our wide range spread to the retail industries and corporate market sectors.</h6>
					<p>Beauty Kasatama&rsquo;s manufacturing footprint includes the most active non-woven processing facility in East Java operating from Gresik and Surabaya.&nbsp;</p>
					<p>The facilities are supplying world class medical - hygiene related supplies products<br />at short lead times. Beauty Kasatama&rsquo;s monthly capacity exceeds more than 50 tons.</p>',

		'title_range_product' => 'OUR RANGE OF PRODUCTS',

		// quality
		't_left_quality1' => '<h1 class="lineh1">OUR<br>QUALITY<br><b>COMMITMENT</b></h1>
					<div class="clear height-20"></div>
					<h6>Beauty Kasatama maintains a significant investment in research and development with a committed team of professionals in its quality control department who use accredited testing facilities to support the perfect creation of medical - hygiene product related supplies.</h6>
					<p>Beauty Kasatama understands the Indonesian market and its conditions and this has made us as one of Indonesia&rsquo;s leading manufacturer of medical and hygiene related products.</p>

					<p>Products made from Beauty Kasatama production standard will deliver high performance. From surgical mask, multipurpose mask, surgical head cap, underpad and other products, all of them will be built strong, effective and attractive.</p>

					<p>Beauty Kasatama&rsquo;s factory offers choices and combinations of useful material and characteristics that passed the international standards in terms of safety and quality.</p>',
		
		'title_quality_value'=>'BEAUTY KASATAMA QUALITY VALUE',
		'quality_val_1' => 'CONSISTENCY',
		'quality_val_2' => 'ZERO DEFECTS',
		'quality_val_3' => 'VERSATILE',

		'quality_passion_title'=>'QUALITY IS - OUR PASSION',
		'quality_passion_subtitle'=>'A desire to improve comprehensive public<br>health in general.',
		'quality_passion_desc' => '<h4>Beauty Kasatama extensive experience, production facility and qualified human resource will enable us to bring the best outcome. Beauty Kasatama will work with high consistency, timely and reliable. We will utilise our possession of automated technology in most of our process to meet our customers requirement.</h4>

					<p>Whether you require a standard high quality products or a custom specification products made exclusively just for your business, Beauty Kasatama will be your best partner to accommodate it.</p>
					<p>Beauty Kasatama will provide the right product with the right process, without sacrificing any stage of quality procedure. The people at Beauty Kasatama are passionate about ensuring you get the very best service, the best product, whatever the size of the business project. From your first enquiry through to execution and beyond, you&#39;ll have a team of dedicated customer relation staffs that are committed working tirelessly to ensure a successful delivery.</p>',

		'quality_pillar_title'=>'THE REASON WE GET<br />
								TO WHERE WE ARE RIGHT NOW<br />
								IS OUR <strong>FOUNDATION PILLARS</strong>',

		'quality_pillar_stitle1'=>'<h3>PEOPLE</h3>',
		'quality_pillar_sinfo1'=>'<p>The people behind the company is our key to success. Beauty Kasatama is always strengthening the team through ongoing investment in leadership, staff development, accountability and empowerment of our teams throughout the organization. With our solid team of dedicated staffs, we look up to the future with confidence.</p>',

		'quality_pillar_stitle2'=>'<h3>INNOVATION</h3>',
		'quality_pillar_sinfo2'=>'<p>Out customer&rsquo;s request are always evolving as is the way they find comfort. In addition to innovative products and services, clients increasingly look for new and easier ways to obtain what they needed.</p>',

		'quality_pillar_stitle3'=>'<h3>EFFICIENCY</h3>',
		'quality_pillar_sinfo3'=>'<p>The financial struggle in Indonesia is very competitive with client needs becoming ever more sophisticated. We are making creative and efficient investments in our manufacturing facility technology in order to allow us to efficiently produce products while controlling our operational costs.</p>',

		'quality_pillar_stitle4'=>'<h3>CONSCIOUSNESS</h3>',
		'quality_pillar_sinfo4'=>'<p>Our customers can rest assured knowing that Beauty Kasatama is a company with self-conciousness of quality and care of everything related to the resources that supports the creation of superior products that exceptional in quality and always consistent in every part of production process.</p>',

		// t_left contact
		't_left_contact1' => '<h1>WE&rsquo;RE<br /><strong>AT YOUR SERVICE</strong></h1>
							<div class="clear height-20"></div>
							<h6>We&#39;d like you to experience and to know more about us. A site tour to our factory / warehouse will be a proof of<br>what we&#39;re stating. Please contact our representative to schedule your site visit. We&#39;ll be happy to assist you.</h6>',

		'OFFICE' => 'OFFICE',
		'view_location' => 'VIEW LOCATION',
		'Phone' => 'Phone',
		'FACTORY' => 'FACTORY',

		't_top_form' => 'Please fill out this form below, so that we can contact you for further discussions.',

		// Footer
		'Address' => 'Address',

		'NAMA'=>'NAME',
		'EMAIL'=>'EMAIL',
		'TELEPON'=>'PHONE',
		'NAMA_USAHA'=>'COMPANY',
		'ALAMAT'=>'ADDRESS',
		'KOTA'=>'CITY',
		'PESAN'=>'MESSAGES',
	);


?>