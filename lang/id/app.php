<?php 

return array(
		'home' => 'HOME',
		'about' => 'TENTANG KAMI',
		'products' => 'PRODUK',
		'quality' => 'KUALITAS',
		'contact' => 'KONTAK',

		'tagline_header_t1'=>'Produk Non Woven Sekali Pakai<br>Didesain Profesional Untuk Memaksimalkan <b>Perlindungan & Kenyamanan</b>',

		// HOME
		't_fcs_m' => 'Beauty Kasatama berkomitmen untuk memberikan produk bertaraf profesional dengan jaminan sterilisasi yang sesuai dengan standar internasional untuk sterilisasi yang efektif.',
		'lebih_lanjut' => 'Read More',

		't_banner_hm_left' => 'Lengkapi informasi kontak Anda dan staf representatif kami akan melayani pertanyaan Anda.',
		'your_name' => 'NAMA LENGKAP',
		'email' => 'EMAIL',
		'mobile_number' => 'No. HP',

		'hm_banner_link1' => 'LIHAT DAFTAR PRODUK',
		'hm_banner_link2' => 'LIHAT PROSES PRODUKSI KAMI',
		'hm_banner_link3' => 'KUNJUNGI PABRIK DIAPRO HEALTHCARE',

		't_bottom_home_banner' => 'Hal-hal yang menjadikan produk Beauty Kasatama<br /><b>lebih baik dari yang lainnya</b>',

		't_bottom_home_bann1' => 'Produk yang sepenuhnya<br />dibuat sesuai pesanan',
		't_bottom_home_bann2' => 'Profesionalisme pelayanan<br />yang sudah teruji',
		't_bottom_home_bann3' => 'Selalu terdepan<br />dalam teknologi',
		
		't_bottom_home_bann_quality' => 'Sertifikasi Kualitas Kami',

		't_bottom_homblue_p' => 'Kekuatan kompetitif Beauty Kasatama terletak pada komitmen kami untuk menjadi yang terbaik<br />dalam merespon dan mengungguli standar internasional untuk mencapai kesempurnaan kualitas.',
		't_bottom_homblue_link' => 'Mari diskusikan dengan kami, Kontak Kami',

		// about
		'ABOUT' => 'TENTANG KAMI',

		'about1_t_left' =>'<h6>Pertama kali berdiri pada tahun 2003 di Kapasari - Surabaya, Beauty Kasatama tadinya merupakan industri rumahan yang memproduksi masker serba guna yang ditujukan untuk masyarakat kelas menengah ke bawah yang menghabiskan aktivitas sehari-harinya di area yang berpolusi yang dapat membahayakan kesehatan mereka.</h6>
							<span>Dengan tekad untuk memperbaiki kesehatan masyarakat, Beauty Kasatama memproduksi produk yang ekonomis dan juga awet yang memberikan manfaat untuk penggunanya.</span>',
		'about1_t_right' =>'<p><em>Sudah menjadi komitmen kami untuk dapat memberikan yang terbaik bagi kesehatan para konsumen, sehingga pada akhirnya kami juga dapat memberikan dampak yang positif untuk masyarakat pada umumnya.</em></p>',

		'about2_t_right' => '<p>Passion perusahaan kami adalah untuk menghadirkan produk terbaik bagi pelanggan yang menyadari pentingnya kesehatan. Keunggulan inilah yang telah dikenal masyarakat luas dan membantu kami untuk berkembang lebih pesat dari sebelumnya. Di tahun 2009, Beauty Kasatama telah melebarkan sayap dan sukses mendirikan gudang produksi ke-2 untuk menjangkau lebih banyak permintaan domestik untuk produk kesehatan/medis yang steril berbahan non-woven.</p>

			<p><strong>Saat ini produk kami telah menjadi salah satu pemimpin pasar di Indonesia. Produk kami juga telah mencapai distribusi internasional dengan beberapa rekanan bisnis asing. Selain itu, kami juga membuka peluang untuk produksi pesanan sesuai permintaan klien.</strong></p>

			<p>Untuk saat ini, kami sedang membangun jaringan bisnis yang lebih luas dan tersebar di seluruh dunia. Kami optimis dan percaya bahwa memproduksi produk berkualitas unggul dengan konsisten akan sekaligus mengenalkan diri kami pada masyarakat luas.</p>',

		't_vision'=>'VISI',
		'tdesc_vision'=>'<p>Menjadi pemimpin pasar produk premium yang menentukan standar kualitas produk non-woven - membangun brand yang disegani yang dikenal atas kualitas dan konsistensinya.</p>',

		't_mission'=>'MISI',
		'tdesc_mission'=>'<p>Mempromosikan kegunaan bahan non-woven sebagai pilihan bahan yang tahan lama, menyasar industri kesehatan, medis dan kebersihan sekaligus melebarkan industri secara berkelanjutan yang fokus untuk memberikan manfaat pada kesehatan masyarakat, serta menjadi pilihan utama penyedia produk bagi seluruh pelanggan.</p>',

		'about_bottom_black_title' => 'KAMI ADALAH<br />
										<strong>BEAUTY KASATAMA</strong><br />
										DAN KAMI HADIR<br />
										UNTUK MEMBANTU ANDA',
		'about_bottom_black_desc' => '<p><strong>Baris Depan, dari kiri ke kanan:</strong><br />
										Tan Roy Tanusudiro - Marketing Director, Lim Kevin Gartnedi Halim - Operational Director, Audi Pascalis Umboh - Head of Export Division</p>

										<p><strong>Baris Belakang, dari kiri ke kanan:</strong><br />
										Management Team - Ahmad Fathoni, Ahmad Nurudin, Abdul Manaf, Nurchamid, Moelyono</p>',

		// product

		't_left_product' => '<h1>PRODUK<br><b>KAMI</b></h1>
					<div class="clear height-10"></div>
					<h6>Sebuah pusat distribusi berjaringan regional yang strategis dengan ketersediaan barang yang lengkap merupakan kunci kami untuk mencapai jangkauan pemasaran produk secara ecer dan sektor pasar perusahaan.</h6>
					<p>Tahapan produksi Beauty Kasatama meliputi fasilitas proses non-woven paling aktif di Jawa Timur yang dioperasikan dari Gresik dan Surabaya.</p>
					<p>Fasilitas-fasilitas ini menghasilkan produk medis - kebersihan berkualitas dunia dalam waktu singkat. Kapasitas produksi Beauty Kasatama melebihi 50 ton setiap bulannya.</p>',

		'title_range_product' => 'DAFTAR PRODUK-PRODUK KAMI',

		// quality
		't_left_quality1' => '<h1 class="lineh1">KOMITMEN<br>KUALITAS<br><b>KAMI</b></h1>
					<div class="clear height-20"></div>
					<h6>Beauty Kasatama mengembangkan riset dan pengembangan dengan tim profesional di departemen kendali kualitas yang menggunakan fasilitas pengujian terakreditasi untuk mendukung pembuatan persediaan produk yang terkait medis - kebersihan.</h6>
					<p>Beauty Kasatama memahami pasar Indonesia dan kondisinya serta telah menjadikan kami salah satu penghasil utama di Indonesia untuk produk medis dan kebersihan.</p>
					<p>Produk-produk yang berasal dari standar produksi Beauty Kasatama akan memiliki performa yang tinggi. Dari masker untuk operasi, masker serbaguna, penutup kepala untuk operasi, alas tempat tidur dan produk-produk lainnya, semuanya dibentuk dengan kuat, efektif serta menarik.</p>
					<p>Pabrik Beauty Kasatama menawarkan pilihan dan kombinasi dari bahan-bahan bermanfaat serta karakteristik yang memenuhi standar internasional keamanan dan kualitas.</p>',
		
		'title_quality_value'=>'NILAI KUALITAS BEAUTY KASATAMA',
		'quality_val_1' => 'KONSISTENSI',
		'quality_val_2' => 'TANPA CACAT',
		'quality_val_3' => 'SERBAGUNA',

		'quality_passion_title'=>'KUALITAS ADALAH SEMANGAT KAMI',
		'quality_passion_subtitle'=>'Sebuah tekad untuk meningkatkan kesehatan<br> masyarakat secara umum.',
		'quality_passion_desc' => '<h4>Pengalaman Beauty Kasatama yang luas, fasilitas produksi dan sumber daya manusia yang berkualitas memungkinkan kami untuk menghadirkan hasil terbaik. Beauty Kasatama akan bekerja dengan konsistensi tinggi, tepat waktu dan dapat diandalkan. Kami akan memanfaatkan teknologi otomatis yang kami miliki di sebagian besar proses kami untuk memenuhi permintaan pelanggan kami.</h4>

					<p>Baik produk dengan standar kualitas tinggi atau produk dengan spesifikasi tertentu yang dibuat khusus untuk bisnis Anda, Beauty Kasatama akan menjadi rekanan terbaik yang akan mengakomodasikannya.</p>
					<p>Beauty Kasatama akan menyediakan produk yang tepat dengan proses yang tepat, tanpa mengorbankan tahapan prosedur kualitas. Kami di Beauty Kasatama bersemangat dalam memastikan pelanggan mendapatkan pelayanan terbaik, produk terbaik, seberapapun ukuran proyek bisnisnya. Mulai dari permintaan awal hingga eksekusi, Anda akan dilayani oleh tim yang berdedikasi dan berkomitmen untuk bekerja tanpa lelah memastikan hanya produk terbaik yang akan diterima pelanggan kami.</p>',

		'quality_pillar_title'=>'ALASAN YANG MENJADIKAN KAMI<br />
								BERADA DI POSISI SAAT INI<br />
								ADALAH <strong>PILAR-PILAR TUMPUAN</strong>',

		'quality_pillar_stitle1'=>'<h3>SUMBER DAYA MANUSIA</h3>',
		'quality_pillar_sinfo1'=>'<p>Orang-orang di balik perusahaan merupakan kunci kesuksesan kami. Beauty Kasatama selalu memperkuat tim melalui investasi berkelanjutan dalam hal kepemimpinan, pengembangan karyawan, pelatihan tanggung jawab dan pemberdayaan tim kami melalui organisasi. Dengan tim solid yang terdiri dari karyawan yang berdedikasi, kami mantap melihat ke depan.</p>',

		'quality_pillar_stitle2'=>'<h3>INOVASI</h3>',
		'quality_pillar_sinfo2'=>'<p>Permintaan pelanggan kami selalu berkembang sebagai bagian dari upaya mencari kenyamanan. Selain inovasi produk dan pelayanan, klien akan mencari cara baru dan lebih mudah untuk mendapatkan apa yang dibutuhkan.</p>',

		'quality_pillar_stitle3'=>'<h3>EFISIENSI</h3>',
		'quality_pillar_sinfo3'=>'<p>Persaingan finansial di Indonesia sangatlah kompetitif dengan kebutuhan klien yang semakin canggih. Kami membuat investasi kreatif dan efisien dalam teknologi fasilitas produksi kami yang memungkinkan kami untuk memproduksi produk dengan efisien sambil mengendalikan biaya operasional kami.</p>',

		'quality_pillar_stitle4'=>'<h3>KESADARAN</h3>',
		'quality_pillar_sinfo4'=>'<p>Pelanggan dapat mempercayakan pada kami bahwa Beauty Kasatama merupakan perusahaan dengan kesadaran akan kualitas dan peduli pada hal-hal yang berkaitan dengan sumber daya yang mendukung terciptanya produk berkualitas terbaik dan selalu konsisten di setiap bagian proses produksi.</p>',

		// t_left contact
		't_left_contact1' => '<h1>KAMI<br /><strong>SIAP MELAYANI ANDA</strong></h1>
							<div class="clear height-20"></div>
							<h6>Kami ingin Anda mengalami dan mengenal kami lebih jauh Kunjungan ke lokasi pabrik
								atau gudang kami akan menjadi bukti pernyataan kami Mohon hubungi representatif kami untuk menjadwalkan kunjungan Anda. Kami akan dengan senang hati mendampingi.</h6>',

		'OFFICE' => 'KANTOR',
		'view_location' => 'LIHAT LOKASI KAMI',
		'Phone' => 'TELPON',
		'FACTORY' => 'PABRIK',

		't_top_form' => 'Silahkan isi formulir di bawah ini, supaya kami dapat menghubungi Anda untuk diskusi lebih lanjut.',

		// Footer
		'Address' => 'Address',

		'NAMA'=>'NAMA',
		'EMAIL'=>'EMAIL',
		'TELEPON'=>'TELEPON',
		'NAMA_USAHA'=>'PERUSAHAAN',
		'ALAMAT'=>'ALAMAT',
		'KOTA'=>'KOTA',
		'PESAN'=>'PESAN',
	);

?>